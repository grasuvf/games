$(document).ready(function() {
  var gameBoard = $('.gameboard'),
    move = 'x';
    squares = [null, null, null, null, null, null, null, null, null],
    lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

  gameBoard.find('.square').on('click', function(){
    if(!$(this).hasClass('full')){
	  var ticTacToeVal = ticTacToe(squares);
      $(this).addClass('full');
      $(this).html(move);
      squares[$(this).attr('data-index')] = move;

      if(ticTacToeVal !== false) {
        $('.win').show().html(ticTacToeVal+' wins!');
        $('.square').addClass('full');
      } else {
        if(squares.indexOf(null) === -1) {
          $('.win').show().html('no one wins!');
        }
        move = move === 'x' ? 'o' : 'x';
      }
    }
  });

  function ticTacToe(marks) {
    for(var i=0;i<lines.length;i++){
      if(marks[lines[i][0]] !== null && marks[lines[i][0]] === marks[lines[i][1]] && marks[lines[i][0]] === marks[lines[i][2]]) {
	    $('.square[data-index='+lines[i][0]+']').addClass('green');
	    $('.square[data-index='+lines[i][1]+']').addClass('green');
	    $('.square[data-index='+lines[i][2]+']').addClass('green');
        return marks[lines[i][0]];
      }
    }

    return false;
  }
});