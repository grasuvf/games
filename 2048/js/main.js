$(document).ready(function () {
  var gameBoard = $('.gameboard');
  var pieces = {};
  var score = 0;
  var gameStarted = false;
  var isGameOver = false;

  drawGrid();
  initiateGame();

  gameBoard.touch({
    preventDefault: {
      drag: true,
      swipe: true,
      dragThreshold: 5,
      dragDelay: 100,
      swipeThreshold: 5,
      tapDelay: 100,
    }
  });
  gameBoard
    .on('swipeUp', function (event) {
      movePieces('up');
    })
    .on('swipeRight', function (event) {
      movePieces('right');
    })
    .on('swipeDown', function (event) {
      movePieces('down');
    })
    .on('swipeLeft', function (event) {
      movePieces('left');
    });

  $(document).on("click", ".gameboard.game-over .reset button", function (e) {
    e.preventDefault();
    resetPieces();
  });

  // initiate game
  function initiateGame() {
    resetPieces();
    addRandomPiece();
    addRandomPiece();
  }

  // draw grid
  function drawGrid() {
    for (var i = 1; i <= 4; i++) {
      gameBoard.find('.grid').append('<div class="row"></div>');
    }

    gameBoard.find('.grid .row').each(function () {
      for (var j = 1; j <= 4; j++) {
        $(this).append('<div class="cell"></div>');
      }
    });
  }

  //reset pieces
  function resetPieces() {
    isGameOver = false;
    gameBoard.removeClass('game-over');
    for (var i = 0; i < 16; i++) {
      pieces[10 * ((i / 4 | 0) + 1) + ((i % 4) + 1)] = null;
    }
  }

  // add random value of 2 or 4 to a random empty cell
  function addRandomPiece() {
    var emptyCellsGridKeys = Object.keys(pieces).filter(function (key) {
      return pieces[key] == null;
    });
    var randomIndex = Math.random() * emptyCellsGridKeys.length | 0;
    var randomKey = emptyCellsGridKeys[randomIndex];
    var randomValue = Math.random() < 0.9 ? 2 : 4;

    pieces[randomKey] = {
      value: randomValue
    };

    if (emptyCellsGridKeys.length === 0) {
      /*noEmptySpaces = true;*/
      return;
    }

    // insert new piece
    gameBoard.find('.pieces').append('<div class="piece piece-new piece_' + randomValue + ' piece-position_' + randomKey + '"></div>');
  }

  function movePieces(direction) {
    if (!gameStarted) {
      gameStarted = true;
      gameBoard.removeClass('start-game');
    }
    if (!isGameOver) {
      moveTo(direction);
      addRandomPiece();
      gameBoard.find('.pieces .piece-to-delete').remove();
    } else {
      gameBoard.addClass('game-over');
    }
  }

  function moveTo(direction) {
    var c_idx, n_idx, f_line;

    if (direction === 'left') {
      for (var i = 1; i <= 4; i++) {
        f_line = false;
        for (var j = 1; j < 4; j++) {
          c_idx = 10 * i + j;
          if (f_line) {
            break;
          }
          for (var k = j + 1; k <= 4; k++) {
            n_idx = 10 * i + k;
            if (pieces[n_idx] !== null) {
              if (pieces[c_idx] === null) {
                pieces[c_idx] = {
                  value: pieces[n_idx].value
                };
                gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                pieces[n_idx] = null;
                if (k !== 4) {
                  j--;
                }
              } else {
                if (pieces[c_idx].value === pieces[n_idx].value) {
                  pieces[c_idx] = {
                    value: pieces[n_idx].value + pieces[c_idx].value
                  };
                  calculateScore(pieces[c_idx].value);
                  pieces[n_idx] = null;
                  gameBoard.find('.pieces .piece-position_' + c_idx).addClass('piece-to-delete');
                  gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                }
              }
              break;
            }
            if (k === 4) {
              f_line = true;
            }
          }
        }
      }
    }
    if (direction === 'right') {
      for (var i = 1; i <= 4; i++) {
        f_line = false;
        for (var j = 4; j > 1; j--) {
          c_idx = 10 * i + j;
          if (f_line) {
            break;
          }
          for (var k = j - 1; k >= 1; k--) {
            n_idx = 10 * i + k;
            if (pieces[n_idx] !== null) {
              if (pieces[c_idx] === null) {
                pieces[c_idx] = {
                  value: pieces[n_idx].value
                };
                gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                pieces[n_idx] = null;
                if (k !== 1) {
                  j++;
                }
              } else {
                if (pieces[c_idx].value === pieces[n_idx].value) {
                  pieces[c_idx] = {
                    value: pieces[n_idx].value + pieces[c_idx].value
                  };
                  calculateScore(pieces[c_idx].value);
                  pieces[n_idx] = null;
                  gameBoard.find('.pieces .piece-position_' + c_idx).addClass('piece-to-delete');
                  gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                }
              }
              break;
            }
            if (k === 1) {
              f_line = true;
            }
          }
        }
      }
    }

    if (direction === 'up') {
      for (var i = 1; i <= 4; i++) {
        f_line = false;
        for (var j = 1; j < 4; j++) {
          c_idx = 10 * j + i;
          if (f_line) {
            break;
          }
          for (var k = j + 1; k <= 4; k++) {
            n_idx = 10 * k + i;
            if (pieces[n_idx] !== null) {
              if (pieces[c_idx] === null) {
                pieces[c_idx] = {
                  value: pieces[n_idx].value
                };
                gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                pieces[n_idx] = null;
                if (k !== 4) {
                  j--;
                }
              } else {
                if (pieces[c_idx].value === pieces[n_idx].value) {
                  pieces[c_idx] = {
                    value: pieces[n_idx].value + pieces[c_idx].value
                  };
                  calculateScore(pieces[c_idx].value);
                  pieces[n_idx] = null;
                  gameBoard.find('.pieces .piece-position_' + c_idx).addClass('piece-to-delete');
                  gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                }
              }
              break;
            }
            if (k === 4) {
              f_line = true;
            }
          }
        }
      }
    }
    if (direction === 'down') {
      for (var i = 1; i <= 4; i++) {
        f_line = false;
        for (var j = 4; j > 1; j--) {
          c_idx = 10 * j + i;
          if (f_line) {
            break;
          }
          for (var k = j - 1; k >= 1; k--) {
            n_idx = 10 * k + i;
            if (pieces[n_idx] !== null) {
              if (pieces[c_idx] === null) {
                pieces[c_idx] = {
                  value: pieces[n_idx].value
                };
                gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                pieces[n_idx] = null;
                if (k !== 1) {
                  j++;
                }
              } else {
                if (pieces[c_idx].value === pieces[n_idx].value) {
                  pieces[c_idx] = {
                    value: pieces[n_idx].value + pieces[c_idx].value
                  };
                  calculateScore(pieces[c_idx].value);
                  pieces[n_idx] = null;
                  gameBoard.find('.pieces .piece-position_' + c_idx).addClass('piece-to-delete');
                  gameBoard.find('.pieces .piece-position_' + n_idx).removeClass().addClass('piece piece_' + pieces[c_idx].value + ' piece-position_' + c_idx);
                }
              }
              break;
            }
            if (k === 1) {
              f_line = true;
            }
          }
        }
      }
    }
  }

  function calculateScore(collected) {
    score = score + collected;
    gameBoard.find('.score').html(score + '<span>+' + collected + '</span>');
  }
});