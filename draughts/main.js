$(document).ready(function() {
  var gameBoard = $('.gameboard'),
    squareNumber = 8,
    startColor = 'black',
    activeColor = startColor,
    activePiece = null,
    piecesSetupInit = {
      piecesOnSquareType: 'black',
      piecesRows: 3 //must be lower or equal then squareNumber/2 - 1
    };

  buildGameBoard();
  initialPiecesSetup();

  gameBoard.find('.square').on('click', function(){
    if($(this).hasClass('piece '+activeColor)) {
      activePiece = $(this);

      if(!activePiece.hasClass('active')) {
        gameBoard.find('.square').removeClass('valid active');
        generateValidMoves(activePiece);
      }
    } else {
      if ($(this).hasClass('valid')) {
        gameBoard.find('.square').removeClass('valid active');
        movePiece(activePiece, $(this));

        if(hasMoreMoves(activePiece, $(this))) { console.log('true');
          activePiece = $(this);
          gameBoard.find('.square').removeClass('valid active');
          generateValidMoves(activePiece);
        } else {
          activePiece = null;
          activeColor = activeColor === 'white' ? 'black' : 'white';
        }
      }
    }
  });

  function buildGameBoard() {
    for(var i=0; i<squareNumber; i++){
      gameBoard.append('<div class="row"></div>');
    }

    gameBoard.find('.row').each(function(i, val){
      for(var j=0; j<squareNumber; j++){
        $(this).append('<div class="square" data-index="'+i+'-'+j+'"></div>');
      }
    });
  }

  function initialPiecesSetup() {
    var initialPosition = {
      "white": [],
      "black": []
    };

    for(var i=0;i<piecesSetupInit.piecesRows;i++){
      for(var j=0;j<squareNumber;j++){
        if((i%2 === 0 && j%2 !== 0) || (i%2 !== 0 && j%2 === 0)) {
          initialPosition.white.push(i + '-' + j);
        }

        if((i%2 === 0 && j%2 === 0) || (i%2 !== 0 && j%2 !== 0)) {
          initialPosition.black.push((squareNumber - i - 1) + '-' + j);
        }
      }
    }

    piecesOnBoard(initialPosition);
    // colorStartGame(setup, startColor);
  }

  function piecesOnBoard(piecesPosition) {
    $.each(piecesPosition, function (pieceColor, pieces) {
      $.each(pieces, function (j, piece) {
        gameBoard.find('.square[data-index="'+piece+'"]').addClass('piece '+pieceColor);
      });
    });
  }

  function generateValidMoves(piece) {
    var x = parseInt(piece.attr('data-index').split('-')[0]),
        y = parseInt(piece.attr('data-index').split('-')[1]),
        color = piece.hasClass('white') ? 'white' : 'black',
        wx, wy, bx, by;

    piece.addClass('active');

    for(var i=-1;i<=1;i+=2) {
      wx = x + 1;
      wy = y - i;
      bx = x - 1;
      by = y + i;
      if((x+i > 0 && x+i < squareNumber) || (y+i > 0 && y+i < squareNumber)) {
        if(color === 'white') {
          if (!thisPiece(wx, wy).hasClass('piece')) {
            thisPiece(wx, wy).addClass('valid');
          } else {
            for(var i=-2;i<=2;i+=4) {
              wx = x + 2;
              wy = y - i;
              bx = x - 2;
              by = y + i;
              if((x+i > 0 && x+i < squareNumber) || (y+i > 0 && y+i < squareNumber)) {
                if(color === 'white') {
                  if (!thisPiece(wx, wy).hasClass('piece')) {
                    thisPiece(wx, wy).addClass('valid');
                  }
                } else {
                  if (!thisPiece(bx, by).hasClass('piece')) {
                    thisPiece(bx, by).addClass('valid');
                  }
                }
              }
            }
          }
        } else {
          if (!thisPiece(bx, by).hasClass('piece')) {
            thisPiece(bx, by).addClass('valid');
          } else {
            for(var i=-2;i<=2;i+=4) {
              wx = x + 2;
              wy = y - i;
              bx = x - 2;
              by = y + i;
              if((x+i > 0 && x+i < squareNumber) || (y+i > 0 && y+i < squareNumber)) {
                if(color === 'white') {
                  if (!thisPiece(wx, wy).hasClass('piece')) {
                    thisPiece(wx, wy).addClass('valid');
                  }
                } else {
                  if (!thisPiece(bx, by).hasClass('piece')) {
                    thisPiece(bx, by).addClass('valid');
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  function thisPiece(x, y) {
    return gameBoard.find('.square[data-index="' + x + '-' + y + '"]');
  }

  function movePiece(oldSquare, newSquare) {
    var colorOldPiece = oldSquare.hasClass('white') ? 'white' : 'black';

    newSquare.addClass('piece '+colorOldPiece);
    oldSquare.removeClass('piece active '+colorOldPiece);
  }

  function hasMoreMoves(oldSquare, newSquare) {
    console.log(oldSquare.attr('data-index'));
    console.log(newSquare.attr('data-index'));
    console.log('');

    var xOldSquare = parseInt(oldSquare.attr('data-index').split('-')[0]),
      xNewSquare = parseInt(newSquare.attr('data-index').split('-')[0]);

    if(Math.abs(xOldSquare) - Math.abs(xNewSquare) === -2 || Math.abs(xOldSquare) - Math.abs(xNewSquare) === 2) {
      return true;
    } else {
      return false;
    }
  }
});