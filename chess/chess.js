angular.module('chessApp', []).directive('chessGame', chessGame);

function chessGame() {
  return {
    restrict: 'A',
    controller: function ($scope) {
      var state = false;
      //var check = false;
      var colorTurn = 'white';
      var pos = [];

      $scope.columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
      $scope.rows = [1, 2, 3, 4, 5, 6, 7, 8];

      $scope.getInitial = function (col, row) {
        var piece = col + row;

        // white
        if (row == 2) return 'white pawn';
        if (piece == 'A1' || piece == 'H1') return 'white rook';
        if (piece == 'B1' || piece == 'G1') return 'white knight';
        if (piece == 'C1' || piece == 'F1') return 'white bishop';
        if (piece == 'D1') return 'white queen';
        if (piece == 'E1') return 'white king';
        // black
        if (row == 7) return 'black pawn';
        if (piece == 'A8' || piece == 'H8') return 'black rook';
        if (piece == 'B8' || piece == 'G8') return 'black knight';
        if (piece == 'C8' || piece == 'F8') return 'black bishop';
        if (piece == 'D8') return 'black queen';
        if (piece == 'E8') return 'black king';
      }

      $scope.move = function (idx, idxp) {
        var piece = angular.element(document.getElementById('s' + idx + idxp));

        if (!state && !piece.hasClass('empty') && piece.hasClass(colorTurn)) {
          if (piece.hasClass('rook')) generateRookPossibleMoves(idx, idxp);
          if (piece.hasClass('bishop')) generateBishopPossibleMoves(idx, idxp);
          if (piece.hasClass('knight')) generateKnightPossibleMoves(idx, idxp);
          if (piece.hasClass('queen')) {
            generateRookPossibleMoves(idx, idxp);
            generateBishopPossibleMoves(idx, idxp);
          }
          if (piece.hasClass('king')) generateKingPossibleMoves(idx, idxp);
          if (piece.hasClass('pawn')) generatePawnPossibleMoves(idx, idxp);

          angular.element(document.getElementById('s' + idx + idxp)).addClass('move');
          pos.push(idx, idxp, piece[0].className);
          state = true;
        } else {
          if (piece.hasClass('valid')) {
            pos.push(idx, idxp, piece[0].className);
            var initialID = 's' + pos[0] + pos[1];
            var initialC = pos[2];
            var destinationID = 's' + pos[3] + pos[4];
            var destinationC = pos[5];
            angular.element(document.getElementById(initialID)).removeClass(initialC);
            angular.element(document.getElementById(initialID)).addClass('empty chess-sq');
            angular.element(document.getElementById(destinationID)).removeClass('empty');
            angular.element(document.getElementById(destinationID)).removeClass(destinationC);
            angular.element(document.getElementById(destinationID)).addClass(initialC);

            colorTurn = colorTurn == 'white' ? 'black' : 'white';

            check(colorTurn);
          }

          angular.element(document.getElementsByClassName('chess-sq')).removeClass('valid capture move');
          pos.splice(0);
          state = false;
        }
      }

      function check(color) {
        var i, j, piece, king = angular.element(document.querySelectorAll('.' + color + '.king'));
        var ec = color == 'white' ? 'black' : 'white';
        var kingX = parseInt(king[0].id.substring(1, 2));
        var kingY = parseInt(king[0].id.substring(2, 3));

        for (i = -2; i <= 2; i++) {
          for (j = -2; j <= 2; j++) {
            if (i != 0 && j != 0 && (j - i) != 0 && (j - i) != -2 && (j - i) != 2 && (j - i) != 4 && (j - i) != -4) {
              piece = angular.element(document.getElementById('s' + (kingX + i) + (kingY + j)));
              if (piece.hasClass('black') && kingX >= 0 && kingX <= 7 && kingY >= 0 && kingY <= 7) {
                //console.log('s' + (kingX + i) + (kingY + j));
                //console.log('check');
                //return true;
              }
            }
          }
        }
      }

      function generateRookPossibleMoves(idx, idxp) {
        var i, piece = angular.element(document.getElementById('s' + idx + idxp));
        var color = piece.hasClass('white') ? 'white' : 'black';
        for (i = idxp - 1; i >= 0; i--) {
          if (checkPos(idx, i, color) == false) break;
        }
        for (i = idxp + 1; i <= 7; i++) {
          if (checkPos(idx, i, color) == false) break;
        }
        for (i = idx - 1; i >= 0; i--) {
          if (checkPos(i, idxp, color) == false) break;
        }
        for (i = idx + 1; i <= 7; i++) {
          if (checkPos(i, idxp, color) == false) break;
        }
      }

      function generateBishopPossibleMoves(idx, idxp) {
        var i, xy, piece = angular.element(document.getElementById('s' + idx + idxp));
        var color = piece.hasClass('white') ? 'white' : 'black';
        xy = idxp - 1;
        for (i = idx + 1; i <= 7; i++) {
          if (checkPos(i, xy, color) == false) break;
          else xy--;
        }
        xy = idxp + 1;
        for (i = idx - 1; i >= 0; i--) {
          if (checkPos(i, xy, color) == false) break;
          else xy++;
        }
        xy = idxp - 1;
        for (i = idx - 1; i >= 0; i--) {
          if (checkPos(i, xy, color) == false) break;
          else xy--;
        }
        xy = idxp + 1;
        for (i = idx + 1; i <= 7; i++) {
          if (checkPos(i, xy, color) == false) break;
          else xy++;
        }
      }

      function generateKnightPossibleMoves(idx, idxp) {
        var i, j, piece = angular.element(document.getElementById('s' + idx + idxp));
        var color = piece.hasClass('white') ? 'white' : 'black';
        for (i = -2; i <= 2; i++) {
          for (j = -2; j <= 2; j++) {
            if (i != 0 && j != 0 && (j - i) != 0 && (j - i) != -2 && (j - i) != 2 && (j - i) != 4 && (j - i) != -4) {
              if (checkPos((idx + i), (idxp + j), color) == false) continue;
            }
          }
        }
      }

      function generateKingPossibleMoves(idx, idxp) {
        var i, j, piece = angular.element(document.getElementById('s' + idx + idxp));
        var color = piece.hasClass('white') ? 'white' : 'black';
        for (i = -1; i <= 1; i++) {
          for (j = -1; j <= 1; j++) {
            if (!(i == 0 && j == 0)) {
              if (checkPos((idx + i), (idxp + j), color) == false) continue;
            }
          }
        }
      }

      function generatePawnPossibleMoves(idx, idxp) {
        var piece = angular.element(document.getElementById('s' + idx + idxp));
        var c = piece.hasClass('white') ? 'white' : 'black';
        var ec = c == 'white' ? 'black' : 'white';
        var pawnLine = c == 'white' ? 6 : 1;
        var pawnIndxp = c == 'white' ? idxp - 1 : idxp + 1;
        var pawnIndxp2 = c == 'white' ? idxp - 2 : idxp + 2;

        if (angular.element(document.getElementById('s' + (idx - 1) + pawnIndxp)).hasClass(ec)) {
          angular.element(document.getElementById('s' + (idx - 1) + pawnIndxp)).addClass('valid capture');
        }
        if (angular.element(document.getElementById('s' + (idx + 1) + pawnIndxp)).hasClass(ec)) {
          angular.element(document.getElementById('s' + (idx + 1) + pawnIndxp)).addClass('valid capture');
        }
        if (angular.element(document.getElementById('s' + idx + pawnIndxp)).hasClass('empty')) {
          angular.element(document.getElementById('s' + idx + pawnIndxp)).addClass('valid');
          if (angular.element(document.getElementById('s' + idx + pawnIndxp2)).hasClass('empty') && idxp == pawnLine) {
            angular.element(document.getElementById('s' + idx + pawnIndxp2)).addClass('valid');
          }
        }
      }

      function checkPos(x, y, c) {
        var el = angular.element(document.getElementById('s' + x + y));
        var ec = c == 'white' ? 'black' : 'white';
        if (!el.hasClass(c) && x >= 0 && x <= 7 && y >= 0 && y <= 7) {
          if (el.hasClass(ec)) {
            el.addClass('valid capture');
            return false;
          }
          if (el.hasClass('empty')) {
            el.addClass('valid');
          }
        } else return false;
      }
    }
  };
}