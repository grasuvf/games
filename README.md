# Games

Try the games [here](https://fungames-23db9.firebaseapp.com/)

## Tech stack
jQuery, AngularJS, LESS

## Chess
![alt text](_img/chess.jpg)

## Draughts (not finished)
![alt text](_img/draughts.jpg)

## Tictactoe
![alt text](_img/ticTacToe.jpg)

## 2048
![alt text](_img/2048.jpg)
